import random
import string


def generate_hash(count_of_chars=200):
    chars = [*string.ascii_letters, *string.digits]
    hash = ""
    for i in range(count_of_chars):
        hash += random.choice(chars)
    return hash
