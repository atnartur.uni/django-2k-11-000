from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import UserManager as DjangoUserManager, AbstractUser, PermissionsMixin
from django.db import models
from django.db.models import Count, Q, Case, F, When, Value, UniqueConstraint

from web.enums import Role


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class UserManager(DjangoUserManager):
    def create_user(self, username, password=None, **extra_fields):
        user = self.model(email=username)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email=None, password=None, **extra_fields):
        user = self.model(email=email, is_staff=True, is_superuser=True)
        user.set_password(password)
        user.save()
        return user


class User(BaseModel, AbstractBaseUser, PermissionsMixin):
    objects = UserManager()
    USERNAME_FIELD = "email"

    email = models.EmailField(unique=True, verbose_name="Email")
    password = models.CharField(max_length=128, verbose_name="Пароль")
    role = models.CharField(max_length=20, choices=Role.choices, verbose_name="Роль", default=Role.USER)
    avatar = models.ImageField(upload_to="user_avatars/", null=True, blank=True, verbose_name="Аватар")
    telegram_user_id = models.PositiveIntegerField(verbose_name="Telegram user ID", null=True, blank=True)

    @property
    def is_staff(self):
        return self.role == Role.ADMIN

    class Meta:
        verbose_name = "пользователь"
        verbose_name_plural = "пользователи"


class SiteQuerySet(models.QuerySet):
    def annotate_history_count(self):
        return self.annotate(
            history_count=Count("history"),
            history_success_count=Count(
                "history", filter=Q(history__status_code__gte=200, history__status_code__lt=300)
            ),
            history_success_percent=Case(
                When(history_count__gt=0, then=F("history_success_count") / F("history_count") * 100),
                default=Value(0),
            ),
        )


class Site(BaseModel):
    objects = SiteQuerySet.as_manager()

    url = models.URLField(verbose_name="URL")
    name = models.CharField(max_length=200, verbose_name="Название")
    status = models.BooleanField(default=True, verbose_name="Доступен ли сайт сейчас?")
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Пользователь", related_name="sites")

    def __str__(self):
        return self.name

    class Meta:
        constraints = [UniqueConstraint(fields=("url", "user_id"), name="url_user_unique")]
        verbose_name = "сайт"
        verbose_name_plural = "сайты"
        permissions = [("check", "Проверка сайта")]


class SiteHistory(BaseModel):
    site = models.ForeignKey(Site, on_delete=models.CASCADE, verbose_name="Сайт", related_name="history")
    status_code = models.IntegerField(null=True, verbose_name="Код ответа")
    error_response_content = models.TextField(null=True, blank=True, verbose_name="Содержимое ошибочного ответа")

    @property
    def is_success(self):
        return self.status_code is not None and self.status_code >= 200 and self.status_code < 300

    class Meta:
        verbose_name = "запись в истории проверок сайта"
        verbose_name_plural = "история проверки сайта"


class SiteComment(BaseModel):
    text = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    sites = models.ManyToManyField(Site)

    class Meta:
        verbose_name = "комментарий к сайту"
        verbose_name_plural = "комментарии к сайтам"


class TelegramHash(BaseModel):
    hash = models.CharField(max_length=200)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
