import pytest

from web.tests.factories import SiteHistoryFactory


@pytest.fixture
def site_history():
    return SiteHistoryFactory()


def test_success_results(site_history):
    assert site_history.is_success


def test_fail_results(site_history):
    site_history.status_code = 404
    site_history.save()
    assert not site_history.is_success
