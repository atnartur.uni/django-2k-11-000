const gulp = require("gulp");
const rename = require("gulp-rename");
const cssnano = require('gulp-cssnano');
const livereload = require('gulp-livereload');
const sass = require('gulp-sass')(require('sass'));

function buildStyles() {
  return gulp.src("./css/main.css")
      .pipe(rename("main.min.css"))
      .pipe(cssnano())
      .pipe(gulp.dest("./css"))
      .pipe(livereload());
}

function buildScss() {
  return gulp.src("./css/example.scss")
      .pipe(rename("example.min.css"))
      .pipe(sass())
      .pipe(cssnano())
      .pipe(gulp.dest("./css"))
      .pipe(livereload());
}

function buildDjangoFormScss() {
  return gulp.src("./css/djangoForm.scss")
      .pipe(rename("djangoForm.min.css"))
      .pipe(sass())
      .pipe(cssnano())
      .pipe(gulp.dest("./css"))
      .pipe(livereload());
}


function defaultTask(cb) {
  console.log('hello world');
  cb();
}

function watch() {
  livereload.listen();
  gulp.watch('./css/main.css', buildStyles)
  gulp.watch('./css/example.scss', buildScss)
  gulp.watch('./css/djangoForm.scss', buildDjangoFormScss)
}

exports.styles = buildStyles;
exports.default = gulp.parallel(defaultTask, buildDjangoFormScss, buildScss, buildStyles);
exports.watch = watch;
