from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect

from web.forms import RegistrationForm, LoginForm
from web.services.unienv import get_authorization_start_url, get_token, get_profile, login_with_unienv_user_profile
from web.services.user import register_user, create_telegram_auth_hash


def register_view(request):
    context = {"form": RegistrationForm()}
    if request.method == "POST":
        form = RegistrationForm(request.POST, request.FILES)
        context["form"] = form
        if form.is_valid():
            email = form.cleaned_data["email"]
            password = form.cleaned_data["password"]
            avatar = form.cleaned_data["avatar"]
            register_user(email, password, avatar)
            context["message"] = "Регистрация прошла успешно"
    return render(request, "web/register.html", context)


def login_view(request):
    form = LoginForm()
    context = {"form": form, "unienv_auth_link": get_authorization_start_url(request)}
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data["email"]
            password = form.cleaned_data["password"]
            user = authenticate(request, email=email, password=password)
            if user is None:
                form.add_error(None, "Email или пароль неверные")
                context["form"] = form
            else:
                login(request, user)
                next_url = request.POST.get("next")
                if next_url != "":
                    return redirect(next_url)
                return redirect("main")
    return render(request, "web/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("main")


@login_required
def telegram_auth_start(request):
    telegram_hash = create_telegram_auth_hash(request.user)
    login = settings.TELEGRAM_BOT_LOGIN
    hash = telegram_hash.hash
    return redirect(f"https://t.me/{login}?start={hash}")


def unienv_auth_callback(request):
    code = request.GET.get("code")
    token = get_token(request, code)
    profile = get_profile(token)
    login_with_unienv_user_profile(request, profile)
    return redirect("main")
