import pytest
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from web.tests.factories import UserFactory


@pytest.fixture(autouse=True)
def enable_db_for_all_tests(db):
    pass


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def user():
    return UserFactory()


@pytest.fixture
def auth_headers(user):
    token = Token.objects.create(user=user)
    return {"HTTP_AUTHORIZATION": f"Token {token.key}"}
